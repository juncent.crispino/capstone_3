import { useState, useEffect } from 'react'
import UserOrder from '../components/UserOrder'

export default function Orders() {

	const [orders, setOrders] = useState([])

	const fetchOrders = () => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/userOrder`, {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setOrders(data.orders)
		})	
	}
	useEffect(()=>{
		fetchOrders()
	},[])



	return(
		<UserOrder orderData={orders} fetchData={fetchOrders} key={orders._id}/>		
	)
}