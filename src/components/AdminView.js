import {Button, Table, Form, Modal, Container} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import Swal from 'sweetalert2'

export default function AdminView(props){

	const {productsData, fetchProducts} = props
	const [showAdd, setShowAdd] = useState(false)
	const [ showEdit, setShowEdit ] = useState(false)
	const [ name, setName ] = useState("")
	const [ brand, setBrand ] = useState("")
	const [ description, setDescription ] = useState("")
	const [ sku , setSku ] = useState("")
	const [ unit, setUnit ] = useState("")
	const [ unitPrice, setUnitPrice ] = useState("")
	const [ category, setCategory ] = useState("")
	const [ subCategory, setSubCategory ] = useState("")
	const [ availableStock, setAvailableStock] = useState("")
	const [ image, setImage] = useState("")
	const [ products, setProducts ] = useState("")
	const [ id, setId ] = useState("")
	const categoryOptions = [
		"Structural Materials",
		"Architectural Materials",
		"Electrical Materials",
		"Plumbing Materials"
	]
	

	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	const addNewProduct = (e) => {
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				brand: brand,
				description: description,
				sku: sku,
				unit: unit,
				unitPrice: unitPrice,
				category: category,
				subCategory: subCategory,
				availableStock: availableStock,
				image: image,
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				closeAdd()
				setName("")
				setBrand("")
				setDescription("")
				setSku("")
				setUnit("")
				setUnitPrice("")
				setCategory("")
				setSubCategory("")
				setAvailableStock("")
				setImage("")
				fetchProducts()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: `You have successfully added ${name}`
				})
				
			}else{
				fetchProducts()
				Swal.fire({
					title: 'Sorry',
					icon: 'error',
					text: 'Something went wrong'
				})
				
			}
		})
	}
	
	const openEdit = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setBrand(data.brand)
			setDescription(data.description)
			setSku(data.sku)
			setUnit(data.unit)
			setUnitPrice(data.unitPrice)
			setCategory(data.category)
			setSubCategory(data.subCategory)
			setAvailableStock(data.availableStock)
			setImage(data.image)
			setId(data._id)

		})
		setShowEdit(true)
	}

	const closeEdit = () => {
		setName("")
		setBrand("")
		setDescription("")
		setSku("")
		setUnit("")
		setUnitPrice("")
		setCategory("")
		setSubCategory("")
		setAvailableStock("")
		setImage("")
		setShowEdit(false)

	}

	const editProduct = (e) => {
		e.preventDefault()
		fetch(` ${process.env.REACT_APP_API_URL}/products/update/${id}`, {
			method: 'PUT',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				brand: brand,
				description: description,
				sku: sku,
				image: image,
				unit: unit,
				unitPrice: unitPrice,
				category: category,
				subCategory: subCategory,
				availableStock: availableStock,
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchProducts()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: `You have successfully updated ${name}`
				})
				closeEdit()
			}else{
				Swal.fire({
					title: 'Sorry',
					icon: 'error',
					text: 'Something went wrong'
				})
			}
		})
		closeEdit()
		setShowEdit(false)

	}


	useEffect(() => {

		const archiveToggle = (productId, isActive) => {
			fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`,{
				method: 'PUT',
				headers: {
					"Content-Type":"application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					isActive: isActive
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data === true){
					fetchProducts()
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: `Product successfully archive/unarchived.`
					})
				}else{
					fetchProducts()
					Swal.fire({
						title: 'Error',
						icon: 'error',
						text: `Something went wrong.`
					})
				}
			})

		}

		const productsArr = productsData.map(product => {
			return(
				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.brand}</td>
					<td>{product.description}</td>
					<td>{product.sku}</td>
					<td>{product.unit}</td>
					<td>{product.unitPrice}</td>
					<td>{product.category}</td>
					<td>{product.subCategory}</td>
					<td>{product.availableStock}</td>
					<td>
						{product.isActive
							?<span>Available</span>
							:<span>Unavailable</span>
						}
					</td>
					<td>
							<Button variant="warning" size="sm" onClick={()=> openEdit(product._id)} >Update</Button>
							{product.isActive
								?
								<Button variant="danger" className="mr-auto" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
								:
								<Button variant="dark" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>

							}
					</td>
				</tr>
			)
		})

		setProducts(productsArr)

	}, [productsData, fetchProducts])

	return(
		<>
		<Container fluid>
			<div className="text-center my-4">
				<h2>Admin DashBoard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="warning" onClick={openAdd} >Add New product</Button>
				</div>
			</div>
			<Table>
				<thead className="bg-dark text-white text-center">
					<tr>
						<th>Name</th>
						<th>Brand</th>
						<th>Description</th>
						<th>SKU</th>
						<th>Unit</th>
						<th>Unit Price</th>
						<th>Category</th>
						<th>SubCategory</th>
						<th>Stocks</th>
						<th>isActive</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
		</Container>
	{/*Add New Product*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={(e) => addNewProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>Brand</Form.Label>
							<Form.Control type="" value={brand} onChange={e => setBrand(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="" value={description} onChange={e => setDescription(e.target.value)}required/>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>SKU</Form.Label>
							<Form.Control type="" value={sku} onChange={e => setSku(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>Unit</Form.Label>
							<Form.Control type="" value={unit} onChange={e => setUnit(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="courePrice">
							<Form.Label>Unit Price</Form.Label>
							<Form.Control type="number" value={unitPrice} onChange={e => setUnitPrice(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>Category</Form.Label>
							<Form.Control type="string" as="select"  onChange={e => setCategory(e.target.value)} required>
							{
								categoryOptions.map((category, index) => {
									return(
										<>
											<option key="blankChoice" hidden value></option>
											<option key={category} value={category}>{category}</option>

										</>
									)
								})
							}
						</Form.Control>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>SubCategory</Form.Label>
							<Form.Control type="" value={subCategory} onChange={e => setSubCategory(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>Image</Form.Label>
							<Form.Control type="" value={image} onChange={e => setImage(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>Available Stock</Form.Label>
							<Form.Control type="number" value={availableStock} onChange={e => setAvailableStock(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Cancel</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		{/*Edit Product*/}
					<Modal show={showEdit} onHide={closeEdit}>
						<Form onSubmit={(e) => editProduct(e)}>
							<Modal.Header closeButton>
								<Modal.Title>Add product</Modal.Title>
							</Modal.Header>
							<Modal.Body>
								<Form.Group controlId="productName">
									<Form.Label>Name</Form.Label>
									<Form.Control type="" value={name} onChange={e => setName(e.target.value)} required/>
								</Form.Group>
								<Form.Group controlId="productName">
									<Form.Label>Brand</Form.Label>
									<Form.Control type="" value={brand} onChange={e => setBrand(e.target.value)} required/>
								</Form.Group>
								<Form.Group controlId="productDescription">
									<Form.Label>Description</Form.Label>
									<Form.Control type="" value={description} onChange={e => setDescription(e.target.value)}required/>
								</Form.Group>
								<Form.Group controlId="productName">
									<Form.Label>SKU</Form.Label>
									<Form.Control type="" value={sku} onChange={e => setSku(e.target.value)} required/>
								</Form.Group>
								<Form.Group controlId="productName">
									<Form.Label>image</Form.Label>
									<Form.Control type="" value={image} onChange={e => setImage(e.target.value)} required/>
								</Form.Group>
								<Form.Group controlId="productName">
									<Form.Label>Unit</Form.Label>
									<Form.Control type="" value={unit} onChange={e => setUnit(e.target.value)} required/>
								</Form.Group>
								<Form.Group controlId="courePrice">
									<Form.Label>Unit Price</Form.Label>
									<Form.Control type="number" value={unitPrice} onChange={e => setUnitPrice(e.target.value)} required/>
								</Form.Group>
								<Form.Group controlId="productName">
									<Form.Label>Category</Form.Label>
									<Form.Control type="string" as="select" value={category} onChange={e => setCategory(e.target.value)} required>
									{
										categoryOptions.map((category, index) => {
											return(
												<>
													<option key={category} value={category}>{category}</option>

												</>
											)
										})
									}
								</Form.Control>
								</Form.Group>
								<Form.Group controlId="productName">
									<Form.Label>SubCategory</Form.Label>
									<Form.Control type="" value={subCategory} onChange={e => setSubCategory(e.target.value)} required/>
								</Form.Group>
								<Form.Group controlId="productName">
									<Form.Label>Available Stock</Form.Label>
									<Form.Control type="number" value={availableStock} onChange={e => setAvailableStock(e.target.value)} required/>
								</Form.Group>
							</Modal.Body>
							<Modal.Footer>
								<Button variant="secondary" onClick={closeEdit}>Cancel</Button>
								<Button variant="success" type="submit">Submit</Button>
							</Modal.Footer>
						</Form>
					</Modal>
		</>
	)
}