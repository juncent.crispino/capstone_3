import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import NavBar from './components/NavBar'
import Footer from './components/Footer'
import { useState, useEffect } from 'react'
import { UserProvider } from './UserContext'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Products from './pages/Products'
import Orders from './pages/Orders'
import SpecificProduct from './pages/SpecificProduct'
import Cart from './pages/Cart'
import AllOrders from './pages/AllOrders'
import WishList from './pages/WishList'
import ScrollToTop from './components/ScrollToTop'


export default function App() {

  const [cart, setCart] = useState([])
  const [quantity, setQuantity] = useState(0)

    useEffect(()=> {
      if(localStorage.getItem('cart')){
        setCart(JSON.parse(localStorage.getItem('cart')))
      }
    }, [])

  const [ user, setUser ] = useState({
    firstName: null,
    lastName: null,
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.removeItem('token')
    setUser({
      firstName: null,
      lastName: null,
      id: null,
      isAdmin: null
    })
  }
  
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
      
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          firstName: data.firstName,
          lastName: data.lastName,
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  useEffect(()=> {
    if(localStorage.getItem('cart')){
      setCart(JSON.parse(localStorage.getItem('cart')))
    }
  }, [])

  useEffect(()=> {
    let tempQuantity = 0
    if(cart.length === 0){
      setQuantity(0)
        
    }else{
      cart.forEach((item)=> {
        tempQuantity += item.quantity
      })
      setQuantity(tempQuantity)
    }
  }, [cart])
  
  return (
    <UserProvider value={{user, setUser, unsetUser, cart, setCart, quantity}}>
      <Router>
      <ScrollToTop/>
        <NavBar/>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/orders" component={Orders}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/products" component={Products}/>
            <Route exact path="/products/:productId" component={SpecificProduct}/>
            <Route exact path="/cart" component={Cart}/>
            <Route exact path="/allOrders" component={AllOrders}/>
            <Route exact path="/wishList" component={WishList}/>
          </Switch>
        <Footer/>
      </Router>
    </UserProvider>
  );
}


