import {Container} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'
import AdminView from '../components/AdminView'
import UserView from '../components/UserView'

export default function Products() {
	const { user } = useContext(UserContext)
	const [products, setProducts] = useState([])
	const [asUserProducts, setAsUserProducts] = useState([])

	const asUserFetch = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
		.then(res => res.json())
		.then(data => {
			setAsUserProducts(data)
		})
	}
	
	const fetchProducts = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			setProducts(data)
		})
	}

	useEffect(() => {
		fetchProducts()
		asUserFetch()
	}, [])

	return(
		<>
			{(user.isAdmin === true)
				? (
					<Container fluid>
						<AdminView productsData={products} fetchProducts={fetchProducts}/>
					</Container>
				)
				: <UserView productsData={asUserProducts} fetchProducts={asUserFetch}/>
			}
		</>
	)
}