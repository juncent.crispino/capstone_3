import {Carousel, Container, Button} from 'react-bootstrap'
import {useState} from 'react'

export default function CategoryCarousel() {
  const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    };

    return (
      <Container className="my-3">
        <Carousel activeIndex={index} onSelect={handleSelect}>
          <Carousel.Item>
            <img fluid={true.toString()}
              className="d-block w-100"
              src="https://images.unsplash.com/photo-1531834685032-c34bf0d84c77?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=987&q=80"
              alt="First slide"
            />
            <Carousel.Caption>
              <Button variant="warning">Structural Materials</Button>
              <p className="bg-dark p-2 mt-2">Shop Structura Materials</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img fluid={true.toString()}
              className="d-block w-100"
              src="https://images.unsplash.com/photo-1528628226822-6f38ca9687c9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
              alt="Second slide"
            />

            <Carousel.Caption>
              <Button variant="warning">Architectural Materials</Button>
              <p className="bg-dark p-2 mt-2">Shop Architectural Materials</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img fluid={true.toString()}
              className="d-block w-100"
              src="https://images.unsplash.com/photo-1607472586893-edb57bdc0e39?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=967&q=80"
              alt="Third slide"
            />

            <Carousel.Caption>
              <Button variant="warning">Plumbing Materials</Button>
              <p className="bg-dark p-2 mt-2">
               Shop Plumbing Materials
              </p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img fluid={true.toString()}
              className="d-block w-100"
              src="https://images.unsplash.com/photo-1462041866295-e4af004a32ef?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=966&q=80"
              alt="Third slide"
            />

            <Carousel.Caption>
              <Button variant="warning">Electrical Materials</Button>
              <p className="bg-dark p-2 mt-2">
                Shop Electrical Materials
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </Container>
      
    );
}

