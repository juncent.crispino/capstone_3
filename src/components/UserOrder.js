import React, { useContext, } from 'react';
import UserContext from '../UserContext'
import { useState, useEffect} from 'react'
import {  Button,  Form, Modal, Jumbotron, Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import { MdStar } from 'react-icons/md'

export default function UserOrder(props) {

	const{orderData} = props
	const [userReview, setUserReview] = useState("")
	const [productId, setProductId] = useState("")
	const [userRating, setUserRating] = useState("")
	const { user } = useContext(UserContext)
	const [ showEdit, setShowEdit ] = useState(false)
	const [individualTransaction, setIndividualTransaction] = useState([])
	const [asUserProducts, setAsUserProducts] = useState([])
	const [hover, setHover] = useState(null)

	const closeEdit = () => {
		setUserReview("")
		setUserRating("")
		setShowEdit(false)
	}

	const openEdit = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id)
		})
		setShowEdit(true)
	}


	const addReview = (e) => {
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products/sendReview`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				name: (user.firstName + " " + user.lastName),
				rating: userRating,
				userReview: userReview
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
							title: 'Success',
							icon: 'success',
							text: `You have successfully added/updated your review`
				})
				closeEdit()
				setShowEdit(false)
			}else{
				Swal.fire({
							title: 'Sorry',
							icon: 'error',
							text: 'Something went wrong'
						})
				closeEdit()
				setShowEdit(false)
			}
		})
		
	}

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
		.then(res => res.json())
		.then(data => {
			setAsUserProducts(data)
		})
	},[])

	useEffect(() => {
		
		const fetchReview = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/findReview/${productId}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUserRating(data.rating)
			setUserReview(data.userReview)
		})
		openEdit(productId)
	}
		const getIndivTrans = orderData.map((order, index) => {
				return(
					<div key={order._id}>
						<div className="container mb-3">
							<div className="row g-3">
								<div className="col-md-12">
									<div className="card">
										<div className="row g-0">
											<div className="col-md-12 col">
												<div className="card-header">
													<div className="row small">
														<div className="col-lg-4">
															<span className="border bg-secondary rounded-left px-2 text-white">
																Order # 
															</span>
															<span className="border bg-white rounded-right px-2 mr-2">
																{index+1}
															</span>
															<span className="border bg-secondary rounded-left px-2 text-white">
															Date
															</span>
															<span className="border bg-white rounded-right px-2">
																{new Date(order.purchasedOn).toLocaleDateString('en-Us')}
															</span>
														</div>
														<div className="col-lg-3">
															<span className="border bg-secondary rounded-left px-2 text-white">
																Contact # 
															</span>
															<span className="border bg-white rounded-right px-2 mr-2">
																{order.contactNo}
															</span>
														</div>
														<div className="col-lg-5">
															<span className="border bg-secondary rounded-left px-2 text-white">
																Address
															</span>
															<span className="border bg-white rounded-right px-2 mr-2">
																{order.address}
															</span>
														</div>
															
													</div>
												</div>
													{order.products.map(item => {
														return(
															<div key={item.productId}>
																<div className="card-body">
																	<div key={item._id}>
																		<div className="row">
													            			<div className="col-md-5 col-lg-3 col-xl-3">
												            					{asUserProducts.map(product => {
												            							return(
												            								<div key={product._id} className="m-auto">{item.productId === product._id ? <img  className="img-fluid w-100" src={product.image} alt={item.name}/> : null}
												            								</div>
												            							)
												            						})
												            					}
													            			</div>
												        					<div className="col-md-7 col-lg-9 col-xl-9">
												        			            <div className="row">
												        			            	<div className="d-flex justify-content-between">
												        			            		<div>
												        			            			<h5><Link to={`/products/${item.productId}`} className="text-dark text-uppercase">{item.name}</Link></h5>
												        			            			<p className="mb-2 text-muted  small">Brand : {item.brand}</p>
												        			            			<p className="mb-2 text-muted  small">Quantity : {item.quantity}</p>
												        			            			<p className="mb-2 text-muted  small"> Unit : {item.unit}</p>	
												        			            			<p className="mb-2 text-muted  small">Unit Price : {item.unitPrice}</p>	
												        			            			<p className="mb-2 text-muted  small">Amount : Php {item.subtotal}</p>
												        			            			<button className="small addReviewBtn" variant="warning" size="sm" onClick={()=> fetchReview(item.productId)} >Add/Update Review</button>	
												        			            		</div>
												        			            	</div>
												        			            </div>
												        					</div>
													            		</div>	  	   	
																	</div >
																</div>
																<hr className="p-0 m-0"/>
															</div>
														)
													})}	
																										
												<div className="card-footer">
												<div className="row small">
													<div className="col-lg-2">
														<span className="mx-2">Total:</span>
														<span>    
															<b>Php {order.totalAmount}</b>
														</span>	
													</div>
													<div className="col-lg-3">
														<span className="mx-2">Payment Mode:</span>
														<span>    
															<b>{order.paymentMode}</b>
														</span>	
													</div>
													<div className="col-lg-5">	
														<span className="mx-2">Status:</span>
														<span className="text-success">    
															<b>{order.status}</b>
														</span>
													</div>	
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
				)		
		})
		setIndividualTransaction(getIndivTrans)
	}, [orderData, asUserProducts])

	return(
		<>
		{(individualTransaction.length === 0) ? (
			<div >
			<Jumbotron>
				<h1 className="display-4 text-center mb-3">You have no previous orders</h1>
				<Container className="d-flex justify-content-center">
					<Link className="btn btn-warning mx-3" to="/products">Shop products</Link>
				</Container>
			</Jumbotron>	
			</div>
		) : (
			<Container className="mt-4">
			<h4 className="m-3">Orders</h4>
			{individualTransaction}
			</Container>
		)

		}		
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={(e) => addReview(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add/Update Review</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Review</Form.Label>
							<Form.Control type="" value={userReview} onChange={e => setUserReview(e.target.value)}/>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>Rating ({userRating}/5)</Form.Label>

							<div>
								{[...Array(5)].map((star, i) => {
									const ratingValue = i + 1;

									return (
										<label key={ratingValue}>
											<input type="radio" name="rating" value={ratingValue} />
											<MdStar className="star" color={ratingValue <= (hover || userRating) ? "#ffc107" : "#e4e5e9"} size={30} onClick={() => setUserRating(ratingValue)} onMouseEnter={()=>setHover(ratingValue)} onMouseLeave={()=> setHover(null)}/>
										</label>
									)
								})}	

							</div>
							
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Cancel</Button>
						<Button variant="warning" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>	
	)
}
