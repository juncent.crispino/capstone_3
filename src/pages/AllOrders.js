import { useEffect, useState } from 'react'
import { Container, Card, ListGroup } from 'react-bootstrap'
import Accordion from 'react-bootstrap/Accordion'
import Swal from 'sweetalert2'

export default function AllOrders() {

	const [allOrders, setAllOrders] = useState([])
	const [perUserOrder, setPerUserOrder] = useState([])
	
	const fetchAllOrders = () => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/allOrders`, {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setAllOrders(data)
		})
	}

	useEffect(()=>{
		fetchAllOrders()
	}, [])


	useEffect(() => {

		const updateStatus = (orderId, userId, status) => {
			fetch(`${process.env.REACT_APP_API_URL}/orders/updateOrderStatus/${orderId}`, {
				method: 'POST',
				headers: {
					"Content-Type":"application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({

					userId:userId,
					status:status
				})	
			})	
			.then(res => res.json())
			.then(data => {
				if(data === true){
					fetchAllOrders()
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: "Status Successfully Updated"
					})
				}else{
					fetchAllOrders()
					Swal.fire({
						title: 'Error',
						icon: 'error',
						text: `Something went wrong.`
					})
				}
			})
		}

		const allOrdersArr = allOrders.map((order, index) =>{
				return(
					<Accordion key={order._id}>
						<Card key={order.purchasedOn}>
							<Card.Header className="bg-dark">
								<Accordion.Toggle className="p-2" as={Card.Body}  eventKey="0" key={order.firstName}>
									<div className="d-flex justify-content-between">
										<span><div className="text-secondary font-weight-bold">User Name | {order.firstName} {order.lastName}</div></span>
									</div>
						   			<div className="d-flex justify-content-between">
						   				<span><div className="text-secondary font-weight-bold"key={order.userId}>User ID | {order.userId}</div></span>
						   			</div>
						   			
						   		</Accordion.Toggle>
						   	</Card.Header>
						{order.orders.map(indiv => {
							return(
								<div  key={indiv._id}>
									<Accordion.Collapse eventKey="0">
										<Card.Body>
										<div key={indiv.purchasedOn} className="ml-3 border-bottom border-secondary">
											Purchased On | {indiv.purchasedOn}}</div>	
												{indiv.products.map(product => {
												return(
													<div key={product.name}>
														<div className="ml-5">⦿ {product.name}   ({product.quantity} {product.unit}/s)</div>
														
													</div>
											)
											})}
										<div key={indiv.totalAmount} className="ml-3"><u>Total Amount : <strong>Php {indiv.totalAmount}</strong></u></div>
										<div key={indiv.address} className="ml-3"><u>Address : <strong>{indiv.address}</strong></u></div>
										<div key={indiv.paymentMode} className="ml-3"><u>Payment Mode :<strong> {indiv.paymentMode}</strong></u></div>
										<div key={indiv.status} className="ml-3"><u>Status :<strong> {indiv.status}</strong></u></div>
										<div>
											<ListGroup className="mt-2" horizontal>
											  <ListGroup.Item action onClick={(e) => updateStatus(indiv._id, order.userId, "Recieved Order")}>Recieved Order</ListGroup.Item>
											  <ListGroup.Item action onClick={(e) => updateStatus(indiv._id, order.userId, "Order/s Packed")}>Order/s Packed</ListGroup.Item>
											  <ListGroup.Item action onClick={(e) => updateStatus(indiv._id, order.userId, "Order/s on Transit")}>Order/s on Transit</ListGroup.Item>
											  <ListGroup.Item action onClick={(e) => updateStatus(indiv._id, order.userId, "Order/s Delivered")}>Order/s Delivered</ListGroup.Item>
											</ListGroup>
										</div>	
										</Card.Body>		
									</Accordion.Collapse>
								</div>
							)
							
						})}
						</Card>
					</Accordion>
				)
		})
		setPerUserOrder(allOrdersArr)

	}, [allOrders])



	return(
		<Container className="mt-3">
			{perUserOrder}
		</Container>
	)
}