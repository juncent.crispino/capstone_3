import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { MdStar } from 'react-icons/md'
import Rating from 'react-rating'

export default function Reviews({reviewsProp}){

	const [productReview, setProductReview] = useState([])

	useEffect(() => {
		const reviewArr = reviewsProp.map(review =>{

			return(
				<div key={review._id} className="grpreviews">			
					<div>
						<div className="d-flex">
							<h6 className="mr-2 mt-2 "><strong>{review.rating}/5</strong></h6>
								<Rating initialRating={review.rating} readonly emptySymbol={<MdStar className="star" color={"#e4e5e9"} size={15}/>} 
								fullSymbol={<MdStar className="star" color={"#ffc107"} size={15}/>}
								/>	
						</div>
						<div className="reviewer">By {review.name}</div>
					</div>
					<div>
					{review.userReview}	
					</div>	
				</div >
			)
		})
		setProductReview(reviewArr)
	}, [reviewsProp])
	return(
			<>
			<Container>
				<div className="mt-5">
					<div>
						<h4>
							<strong>{reviewsProp.length > 1 ? <>REVIEWS</> : <>REVIEW</>}</strong>
						</h4>
					</div>
					{reviewsProp.length !== 0 ? <>{productReview}</> : <div>No Reviews Yet</div>}
					
				</div>
			</Container>	
			</>
		)
}