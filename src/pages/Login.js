import React, { useState, useEffect, useContext } from 'react'
import { Form, Button, Container} from 'react-bootstrap'
import { Redirect } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login() {

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [loginButton, setLoginButton] = useState(false)
	const { user, setUser } = useContext(UserContext)

	function loginUser(e){
		e.preventDefault()

		fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
			}else{
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again"
				})
			}
		})

	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				firstName: data.firstName,
				lastName: data.lastName,
				id:data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if (email !== '' && password !== ''){
			setLoginButton(true)
		}else{
			setLoginButton(false)
		}
	}, [email, password])

	if(user.id !== null){
		return <Redirect to= "/"/>
	}

 
	return(

		<Container>
			<Form className="mt-3" onSubmit={(e) => loginUser(e)}>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type='email' placeholder='Enter email' value={email} onChange={ e => setEmail(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type='password' placeholder='Enter Password' value={password} onChange={ e => setPassword(e.target.value)} required/>
				</Form.Group>
				{loginButton ?
					<Button variant='warning' type='submit'>Login</Button>
					: 
					<Button variant='warning' type='submit' disabled>Login</Button>
				}
			</Form>
		</Container>

	)
}
