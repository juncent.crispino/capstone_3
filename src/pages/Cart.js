import { Modal, Form, Button, Container, Jumbotron, Toast} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { useState, useEffect, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { CgUnavailable } from "react-icons/cg";
import { BiEditAlt } from "react-icons/bi";
import {FaTrashAlt} from 'react-icons/fa'

export default function Cart(){

	const { user, cart, setCart } = useContext(UserContext)
	const [products, setProducts] = useState([])
	const [total, setTotal] = useState(0)
	const [userDetails, setUserDetails] = useState([])
	const [address, setAddress] = useState("")
	const [paymentMode, setPaymentMode] = useState("")
	const [ showEdit, setShowEdit ] = useState(false)
	const [contactNo, setContactNo ] = useState("")
	const [asUserProducts, setAsUserProducts] = useState([])
	const [showA, setShowA] = useState(false);
	const [editAddress, setEditAddress] = useState(true)
	const [editContactNo, setEditContactNo] = useState(true)
	const paymentOption = [
		"Cash on Delivery",
		"Online Banking",
		"G-Cash",
		"Paymaya"
	]
console.log(asUserProducts)
	const toggleShowA = () => setShowA(!showA);

	const closeEdit = () => {
		setAddress("")
		setPaymentMode("")
		setShowEdit(false)
		setEditAddress(true)
		setEditContactNo(true)

		}

	const asUserFetch = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAsUserProducts(data)
		})
	}

	const userDetailsFetch = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			method: 'POST',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUserDetails(data)
		})
	}

	useEffect(()=> {
		userDetailsFetch()
		asUserFetch()
	},[])
	
	useEffect(()=> {
		let tempTotal = 0
		if(cart.length === 0){
			setTotal(0)
				
		}else{
			cart.forEach((item)=> {
				tempTotal += item.subtotal
			})
			setTotal(tempTotal)
		}

		asUserFetch()

	}, [cart])

	const checkOut = (e) => {
		e.preventDefault()
		fetch(` ${process.env.REACT_APP_API_URL}/orders/checkout `, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				firstName: user.firstName,
				lastName: user.lastName,
				contactNo: contactNo,
				products: cart,
				totalAmount: total,
				address: address,
				paymentMode: paymentMode
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: 'Yaaay!',
					icon: 'success',
					text: `You have successfully placed your order.`
				})
				localStorage.removeItem('cart')	
				setCart([])
			}else{
				alert("hello")
				Swal.fire({
					title: 'Sorry',
					icon: 'error',
					text: 'Something went wrong'
				})
			}
		
		})
		
		closeEdit()
		setShowEdit(false)
	}

	useEffect(() => {
		const addQty = (item) => {
			let tempCart = [...cart]
			let tempProduct = [...asUserProducts]
			let availableStock

			for (let i = 0; i < tempProduct.length; i++){
				if(tempProduct[i]._id === item){
					availableStock = tempProduct[i].availableStock
				}
			}

			for(let i = 0; i < tempCart.length; i++){
				if(tempCart[i].productId === item && tempCart[i].quantity < availableStock ){
				tempCart[i].quantity += 1
				tempCart[i].subtotal = tempCart[i].quantity*tempCart[i].unitPrice
				}else if(tempCart[i].productId === item && tempCart[i].quantity > (availableStock - 1)){
					setShowA(true)
				}
				setCart(tempCart)
			}
			localStorage.setItem('cart', JSON.stringify(cart))
		}

		const subQty = (item) => {

			let tempCart = [...cart]
			for(let i = 0; i < tempCart.length; i++){
				if(tempCart[i].productId === item && tempCart[i].quantity > 1){
				tempCart[i].quantity -= 1
				tempCart[i].subtotal = tempCart[i].quantity*tempCart[i].unitPrice

				}
				setCart(tempCart)
			}
			localStorage.setItem('cart', JSON.stringify(cart))
		}
		const removeItem = (item) => {
			let tempCart = [...cart]
			for(let i = 0; i < cart.length; i++){
				if(cart[i].productId === item){
					tempCart.splice([i], 1)

				}
				setCart(tempCart)
			}
			localStorage.setItem('cart', JSON.stringify(tempCart))
		}
		const qtyInput = (item, value) => {
			let inputValue = parseInt(value)
			let tempCart = [...cart]
			let tempProduct = [...asUserProducts]
			let availableStock

			for (let i = 0; i < tempProduct.length; i++){
				if(tempProduct[i]._id === item){
					availableStock = tempProduct[i].availableStock
				}
			}

			for(let i = 0; i < tempCart.length; i++){
				if(tempCart[i].productId === item  && inputValue <= availableStock){
					tempCart[i].quantity = inputValue
					tempCart[i].subtotal = tempCart[i].unitPrice * tempCart[i].quantity
				}else if(tempCart[i].productId === item && inputValue > availableStock){
					setShowA(true)
					tempCart[i].quantity = availableStock
					tempCart[i].subtotal = tempCart[i].unitPrice * tempCart[i].quantity
				}
				setCart(tempCart)	
			}
			
			localStorage.setItem('cart', JSON.stringify(tempCart))
			
		}

		if(cart.length === 0){	
			return null
		}else{
			const productsArr = cart.map(item => {
				return(
					<div key={item.productId}>
	            		
	            		<div className="row mb-4" >
	            			<div className="col-md-5 col-lg-3 col-xl-3">
            					{asUserProducts.map(product => {
            							return(
            								<div key={product._id} className="">{item.productId === product._id ? <img  className="img-fluid w-100" src={product.image} alt={product.name}/> : null}
            								</div>
            							)
            						})
            					}
	            			</div>
        					<div className="col-md-7 col-lg-9 col-xl-9">
        			            <div>
        			            	<div className="d-flex justify-content-between">
        			            		<div>
        			            			<h5><Link to={`/products/${item.productId}`} className="text-dark text-uppercase">{item.name}</Link></h5>
        			            			<p className="mb-2 text-muted  small">Brand : {item.brand}</p>
        			            			<p className="mb-2 text-muted  small ">
        			            				Available Stock : {asUserProducts.map(product => {
        			            					return (
        			            						<>
        			            							{item.productId === product._id
        			            								? <>{product.availableStock}</>
        			            								: null	
        			            							}
        			            						</>
        			            					)
        			            				})}
        			            			</p>
        			            			<p className="mb-2 text-muted  small"> Unit Price : ₱ {asUserProducts.map(product => {
        			            					return (
        			            						<>
        			            							{item.productId === product._id
        			            								? <>{product.unitPrice}</>
        			            								: null	
        			            							}
        			            						</>
        			            					)
        			            				})}</p>	
        			            			<p className="mb-2 text-muted  small">Unit : {item.unit}</p>	
        			            		</div>
        			            		<div>
        			            			<div className="mt-2 mb-0 w-100">
        			            				<div className="inputgroupsm">
        			           					<button  className="decbtnsm" onClick={() => subQty(item.productId)}>-</button>
        			            					<input id="quantitysm" type="number" min="1" value={item.quantity} className="qtyinputsm" onChange={(e)=> qtyInput(item.productId, e.target.value)} />
        			            					<button  className="incbtnsm" onClick={() => addQty(item.productId)}>+</button>
        			            				</div>
        			            			</div>
        			            			<small id="passwordHelpBlock" className="form-text text-muted text-center">
        			            				{item.quantity > 1 ? <>{item.unit}s</> : <>{item.unit}</>}
        			            			</small>
        			            		</div>
        			            	</div>
        			            	<div className="d-flex justify-content-between align-items-center">
        			            		<div>
        			            			<a href="#!" type="button"  onClick={() => removeItem(item.productId)} variant="secondary"className="text-muted small text-uppercase mr-3"><FaTrashAlt size={15}/> Remove item </a>
        			            		</div>
        			            		<p className="mb-0"><span><strong id="summary" className="mb-0">₱ {item.subtotal.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})}</strong></span></p>
        			            	</div>
        			            </div>
        					</div>
	            		</div>
	            		<hr className="mb-4"/>
						
		            </div>	
				)
			})
			setProducts(productsArr)
		}

	}, [cart, asUserProducts, setCart])

	if(user.isAdmin === true){
		return <Redirect to="/products"/>

	}

	let userOrder = (cart.length === 0) ? (
			
			<div >
				<Jumbotron>
					<h1 className="display-4 text-center mb-3">Your Cart is Empty!</h1>
					<Container className="d-flex justify-content-center">
						<Link className="btn btn-warning mx-3" to="/products">Shop products</Link>
						<Link className="btn btn-dark text-white " to="/orders">View Order</Link>
					</Container>
				</Jumbotron>	
			</div>
		) : (
			<>
				<Container>
					{/*<!--Section: Block Content-->*/}
					<section>
						{/*<!--Grid row-->*/}
						<div className="row mt-5">
							{/*<!--Grid column-->*/}
							<div className="col-lg-8 cartcard">
								{/*<!-- Card -->*/}
								<div className="mb-3">
									<div className="pt-4">
										<div className="d-flex justify-content-between"><h5 >Shopping Cart </h5>
										<div className="text-muted small text-uppercase">(<span>{cart.length}</span>{cart.length > 1 ? <> items</> : <> item</>})</div></div>
										<hr className="mb-4"/>
										{products}
										<p className="text-muted mb-0"><i className="fas fa-info-circle mr-1"></i> Do not delay the purchase, adding items to your cart does not mean booking them.</p>
									</div>
								</div>
								{/*<!-- Card -->*/}
								
								
							</div>
							{/*<!--Grid column-->*/}
							{/*<!--Grid column-->*/}
							<div className="col-lg-4 cartcard">
								
									{/*<!-- Card -->*/}
									<div className="mb-3">
										<div className="pt-4">
											<h5 className="mb-3">Summary</h5>
											<ul className="list-group list-group-flush">
												<li className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
													Amount<span>₱ {total.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})}</span>
												</li>
												<li className="list-group-item d-flex justify-content-between align-items-center px-0">
													Shipping<span>₱ {(500).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})}</span>
												</li>
												<li className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
													<div>
														<strong>Total Amount</strong>
														<strong>
															<p className="mb-0">(including VAT)</p>
														</strong>
													</div>
													<span><strong>₱ {(total + 500).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})}</strong></span>
												</li>
											</ul>
											<button type="button" className="btn btn-warning btn-block" onClick={(e) => checkOutDetails(e)}>go to checkout</button>
										</div>
									</div>
									{/*<!-- Card -->*/}
							
							</div>
							{/*<!--Grid column-->*/}		
						</div>
						{/*<!-- Grid row -->*/}
					</section>
					{/*<!--Section: Block Content-->*/}
				</Container>
			</>
		)

		const checkOutDetails = () => {
			setShowEdit(true)
			setContactNo(userDetails.mobileNo)
			setAddress(userDetails.address)
		}
		
	return(
		<>
		{userOrder}

		<Modal show={showEdit} onHide={closeEdit}>
			<Form onSubmit={(e) => checkOut(e)}>
				<Modal.Header closeButton>
					<Modal.Title>Check Out Details</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group controlId="address">
						<Form.Label>Address</Form.Label>
						<div className="d-flex">
						<div className="w-100">
							<Form.Control type="" disabled={editAddress} value={address}  onChange={e => setAddress(e.target.value)} required/>
						</div>
						<Button variant="secondary ml-1" onClick={e=> setEditAddress(!editAddress)}><BiEditAlt/></Button>
						</div>			
					</Form.Group>
					<Form.Group controlId="address">
						<Form.Label>Address</Form.Label>
						<div className="d-flex">
						<div className="w-100">
							<Form.Control type="" disabled={editContactNo} value={contactNo}  onChange={e => setContactNo(e.target.value)} required/>
						</div>
						<Button variant="secondary ml-1" onClick={e=> setEditContactNo(!editContactNo)}><BiEditAlt/></Button>
							
						</div>
								
					</Form.Group>
					<Form.Label>Payment Mode</Form.Label>
					<Form.Control type="string" as="select" value={paymentMode} onChange={e => setPaymentMode(e.target.value)} required>
						<option key="blankChoice" hidden value></option>
						{
							paymentOption.map((category) => {
								return(
									<>
										
										<option key={category} value={category} required>{category}</option>
									</>
								)
							})
						}
					</Form.Control>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={closeEdit}>Cancel</Button>
					<Button variant="warning" type="submit">Place Order</Button>
				</Modal.Footer>
			</Form>
		</Modal>
		<Toast show={showA} onClose={toggleShowA}  delay={5000} autohide className="toast">
			<Toast.Header className="untoastText">
				<CgUnavailable size={25} className="checkToast"/><strong className="pr-2">
					<span className="ml-2">Not enough Stocks</span>
					</strong>
			</Toast.Header>
		</Toast>

		</>		
	)
}