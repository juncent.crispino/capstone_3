import { Link } from 'react-router-dom'
import {ImFacebook2, ImInstagram, ImTwitter, ImLinkedin} from "react-icons/im";
import UserContext from '../UserContext'
import React, { useContext, } from 'react';


export default function Footer() {
	const { user } = useContext(UserContext)
	return(
		<footer>
			<div className="footerContainer pb-0 mb-0 justify-content-center text-light bg-dark ">
			    <footer>
			        <div className="row my-5 justify-content-center py-5">
			            <div className="col-11">
			                <div className="row ">
			                    <div className="col-xl-8 col-md-4 col-sm-4 col-12 my-auto mx-auto a">
			                        <h3 className="text-muted mb-md-0 mb-5 bold-text">JC's Supply</h3>
			                    </div>
			                    <div className="col-xl-2 col-md-4 col-sm-4 col-12">
			                        <h6 className="mb-3 mb-lg-4 bold-text "><b>MENU</b></h6>
			                        <div className="">
			                            <div>
			                            	<Link className="text-muted" to="/">Home</Link>
			                            </div>
			                            <div>
			                            	<Link className="text-muted" to="/products">Products</Link>
			                            </div>
			                            {user.id !== null ? 
			                            <>
											<div>
												<Link className="text-muted" to="/orders">Orders</Link>
											</div>
											<div>
												<Link className="text-muted" to="/wishList">Wish List</Link>
											</div>
										</> : 
			                            <>
											<div>
												<Link className="text-muted" to="/register">Register</Link>
											</div>
											<div>
												<Link className="text-muted" to="/login">Login</Link>
											</div>
										</>
			                            }
			                        </div>
			                    </div>
			                    <div className="col-xl-2 col-md-4 col-sm-4 col-12">
			                        <h6 className="mb-3 mb-lg-4 text-muted bold-text mt-sm-0 mt-5"><b>ADDRESS</b></h6>
			                        <p className="mb-1">1234 Manila City</p>
			                        <p>National Capital Region</p>
			                    </div>
			                </div>
			                <div className="row ">
			                    <div className="col-xl-8 col-md-4 col-sm-4 col-auto my-md-0 mt-5 order-sm-1 order-3 align-self-end">
			                        <p className="social text-muted mb-0 pb-0 bold-text"> <span className="mx-2"><ImFacebook2 size={30}/></span><span className="mx-2"><ImLinkedin size={30}/></span><span className="mx-2"><ImInstagram size={30}/></span> <span className="mx-2"><ImTwitter size={30}/></span></p><small className="rights"><span>&#174;</span> JC's Supply All Rights Reserved.</small>
			                    </div>
			                    <div className="col-xl-4 col-md-8 col-sm-8 col-auto order-1 align-self-end ">
			                        <h6 className="mt-55 mt-2 text-muted bold-text"><b>JUNCENT A. CRISPINO</b></h6><small> <span><i className="fa fa-envelope" aria-hidden="true"></i></span>juncent.crispino@gmail.com</small>
			                    </div>
			                    
			                </div>
			            </div>
			        </div>
			    </footer>
			</div>
		</footer>
	)
}