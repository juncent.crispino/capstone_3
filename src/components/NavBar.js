import React, { useContext, } from 'react';
import { Navbar, Nav,  Badge } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom'
import UserContext from '../UserContext'


export default function NavBar() {

  const { user, unsetUser, quantity } = useContext(UserContext)
  const history = useHistory()

  const logout = () => {
    unsetUser()
    history.push('/login')
  }

  let rightNav = (!user.id) ? (
      <>
        <Link className="nav-link" to="/register">Register</Link>
        <Link className="nav-link" to="/login">Log In</Link>
      </>
  ) : (user.isAdmin !== true)?(
      <>
        
        <Link className="nav-link" to="/cart">Cart<Badge pill className="bg-warning text-dark ml-1">{quantity}</Badge></Link>
        <Link className="nav-link" to="/wishList">Wish List</Link>
        <Link className="nav-link" to="/orders">Orders</Link>
        <Nav.Link onClick={logout}>Log Out</Nav.Link>
      </>
  ) : (<>
        <Link className="nav-link" to="/allOrders">All Orders</Link>
        <Nav.Link onClick={logout}>Log Out</Nav.Link>
      </>
  )

  return (
    <>
      <Navbar bg="dark"expand="sm" collapseOnSelect variant="dark" className="sticky-top navi">
          <Link className="navbar-brand" to='/'>JC's Supply</Link>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Link className="nav-link" to="/">Home</Link>
              <Link className="nav-link" to="/products">Products</Link>
            </Nav>
            <Nav>
              {rightNav}
            </Nav>
          </Navbar.Collapse>
      </Navbar>
      </>
  );
}

