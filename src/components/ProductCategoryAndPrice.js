import {Link} from 'react-router-dom'
import {FaShoppingCart} from 'react-icons/fa'
import { BsHeartFill } from "react-icons/bs";
import { Button } from 'react-bootstrap'

export default function ProductCategoryAndPrice({item, addToCart, addToWishList, user}) {
	
	return(
		<>
		{item.isActive === true
		?
			<div key={item._id} className="border border-dark itemCard">
				<div>
					<img className="r1" src={item.image} alt={item.name}/>
				</div>
				<div className="cardInfo">
					<div className="itemsName">
						{item.name.slice(0, 20)} . . .
					</div>
					<div className="itemsBrand">
						{item.brand.slice(0, 20)} . . .
					</div>
					
					<div className="soldItem">
						<strong>{item.sold === undefined ? <>0 {item.unit}/s</> : <>{item.sold} {item.unit}/s</>}</strong> Sold
					</div>
					<div className="soldItem">
						<div>
							<strong>Php {item.unitPrice}</strong> / {item.unit}
						</div>
						<div>
							<strong>{item.availableStock} {item.unit}/s </strong>Available
						</div>
					</div>	
				</div>
				<div className="cardButton">
					<div>
						<Link size="small" className="btn btn-dark mr-2" to={`/products/${item._id}`}>Details</Link>
					</div>
					<>
						{user.id !== null
							? 
							<> 
								{item.availableStock !== 0
									? 
									<>
										<div>
											<Button className="btn btn-warning btn-block px-4"  onClick={() => addToCart(item)}><FaShoppingCart className="star" color={"#343a40"} size={20}/></Button>
										</div>
										<div>
											<Button className="btn btn-warning btn-block px-4" onClick={() => addToWishList(item._id)}><BsHeartFill  className="star" color={"#343a40"} size={20}/></Button>
										</div>
									</>
									: 
									<div>
										<Button className="btn btn-warning btn-block" block onClick={() => addToWishList(item._id)}>
											<BsHeartFill  className="star" color={"#343a40"} size={20}/>
										</Button>
									</div>	
								}			
							</>
							:
							<div>
								<Link className="btn btn-danger btn-block " to="/login" ><FaShoppingCart className="star" color={"#343a40"} size={20}/>
								</Link>
							</div>
						}		
					</>
				</div>
			</div>
			: null
			}
		</>	
	)
}