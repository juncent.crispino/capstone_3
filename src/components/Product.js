import { Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Product({productProp}) {
	
	const { _id, name, brand, description, unit, unitPrice, image, availableStock} = productProp

	return(
		<>
			<Card className="mt-5" style={{ width: '18rem' }}>
				  <Card.Img variant="top" src={image} />
				  <Card.Body>
				    <Card.Title className="my-1 font-weight-bold">{name}</Card.Title>
				    <h6 className="my-1 font-weight-bold">{brand}</h6>
				    <h8 className="my-1 font-weight-light">{description}</h8>
				    <Card.Text className="my-1 font-weight-light">{availableStock} Available Stocks</Card.Text>
				    <Card.Text className="my-1 font-weight-light">{unitPrice} / {unit}</Card.Text>
				    <Link className="btn btn-dark" to={`/products/${_id}`}>Details</Link>
				  </Card.Body>
			</Card>
		</>	
	
	)
}



		