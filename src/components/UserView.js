import { useState, useEffect, useContext, useCallback} from 'react'
import {Form, Spinner} from 'react-bootstrap'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import ProductCategoryAndPrice from "./ProductCategoryAndPrice"

export default function UserView({productsData}){

	const { user, setCart } = useContext(UserContext)
	const [productCategory, setProductCategory] = useState([])
	const [itemCardCategory, setItemCardCategory] = useState([])
	const [category, setCategory] = useState("")
	const [minPrice, setMinPrice] = useState(undefined)
	const [maxPrice, setMaxPrice] = useState(undefined)
	const [loading, setLoading] = useState(false)
	const [pageNumber, setPageNumber] = useState(1)

	const fetchSortProducts = useCallback((e) => {
		fetch(` ${process.env.REACT_APP_API_URL}/products/sortProduct?page=${pageNumber}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category: category,
				minprice: minPrice,
				maxprice: maxPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			setProductCategory(data)
			setLoading(true)
		})
	}, [category, minPrice, maxPrice, pageNumber])

	useEffect(()=> {
		fetchSortProducts()
	}, [category, minPrice, maxPrice, fetchSortProducts, pageNumber])

	const handlePrev =()=>{
	        if(pageNumber === 1) return
	        setPageNumber(pageNumber - 1)
	    	setLoading(false)

	    }
    const handleNext =()=>{
        setPageNumber(pageNumber + 1)
        setLoading(false)
    }

    useEffect(()=> {
		window.scrollTo(0, 0);
	}, [pageNumber])

	const addToWishList = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/addToWishList/${productId}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Added to Wish List successfully',
					timer: 4000
				})
			}
		})
	}

	useEffect(()=> {
		setPageNumber(1)
	}, [category, minPrice, maxPrice])	

	useEffect(()=>{
		const addToCart = (product) => {
			let alreadyInCart = false
			let productIndex
			let cart = []

			if(localStorage.getItem('cart')){
				cart = JSON.parse(localStorage.getItem('cart'))
			}

			for(let i = 0; i < cart.length; i++){
				if(cart[i].productId === product._id){
					alreadyInCart = true
					productIndex = i
				}
			}

			if(alreadyInCart){
				if(cart[productIndex].quantity < product.availableStock){
					cart[productIndex].quantity += 1
					cart[productIndex].subtotal = cart[productIndex].unitPrice * cart[productIndex].quantity
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Added to Cart successfully',
						timer: 4000
					})
				}else if(cart[productIndex].quantity > (product.availableStock-1)){
					Swal.fire({
						title: 'Sorry',
						icon: 'error',
						text: `Your cart already have ${cart[productIndex].quantity} ${product.unit}/s, the available stock is only ${product.availableStock} ${product.unit}/s. You can only add ${product.availableStock - cart[productIndex].quantity} ${product.unit}/s in your cart.`,
						timer: 10000
					})
				}	
			}else{
				if(product.availableStock >= 1){
					cart.push({
						'productId' : product._id,
						'name': product.name,
						'brand': product.brand,
						'unit': product.unit,
						'unitPrice': product.unitPrice,
						'quantity': 1,
						'subtotal': product.unitPrice 
					})
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Added to Cart successfully',
						timer: 4000
					})
				}		
			}
			localStorage.setItem('cart', JSON.stringify(cart))
			setCart(cart)			
		}

		const productArr = productCategory.map(item => {
			return (
				<ProductCategoryAndPrice item={item} addToCart={addToCart} addToWishList={addToWishList} user={user} />					
			)						
		})
		setItemCardCategory(productArr)	
	},[productCategory, user, setCart])

	return(
		<>
			<div className="container-fluid my-3">
				<div className="d-flex justify-content-end mb-3 mr-5">Page {pageNumber} </div>
				<div className="d-flex justify-content-end mr-5">
	            	<button className="btn btn-warning" onClick={handlePrev}>prev</button>
	            	<button className="btn btn-warning ml-2" onClick={handleNext}>next</button>       
	            </div>
				<div className="row">
					<div className="col-md-3">
						<div className="card mb-3">
						  <div className="card-header font-weight-bold text-uppercase">
						    Categories
						  </div>
						  <ul className="list-group list-group-flush">
						    <li className="list-group-item">
						      <button  className={category === "" ? "activeBtn" : "notActiveBtn text-muted"} onClick={e => {setCategory(""); setLoading(false);}}>All Products</button>
						    </li>
						    <li className="list-group-item">
						      <button  className={category === "Structural Materials" ? "activeBtn" : "notActiveBtn text-muted"} onClick={e => {setCategory("Structural Materials"); setLoading(false);}}>Structural</button>
						    </li>
						    <li className="list-group-item">
						      <button  className={category === "Architectural Materials" ? "activeBtn" : "notActiveBtn text-muted"} onClick={e => {setCategory("Architectural Materials"); setLoading(false);}}>Architectural</button>
						    </li>
						    <li className="list-group-item">
						      <button  className={category === "Electrical Materials" ? "activeBtn" : "notActiveBtn text-muted"} onClick={e => {setCategory("Electrical Materials"); setLoading(false);}}>Electrical</button>
						    </li>
						    <li className="list-group-item">
						      <button  className={category === "Plumbing Materials" ? "activeBtn" : "notActiveBtn text-muted"} onClick={e => {setCategory("Plumbing Materials"); setLoading(false);}}>Plumbing</button>
						    </li>
						    
						  </ul>
						</div>
						<div className="card mb-3">
						  <div className="card-header font-weight-bold text-uppercase">Price</div>
						  	<div className="">
						  		<Form>
						  		<div className="d-flex align-items-center p-2">
						  		  <Form.Control placeholder="Php Min" type="number" className="qtyinputprice" value={minPrice === "" ? setMinPrice(undefined) : minPrice}  onChange={e => {setMinPrice(e.target.value); setLoading(false)}} required/>
						  		  <label className="small px-2">
						  		   <strong> - </strong>
						  		  </label>
						  		  <Form.Control placeholder="Php Max" type="number" className="qtyinputprice" value={maxPrice === "" ? setMaxPrice(undefined) : maxPrice}  onChange={e => {setMaxPrice(e.target.value); setLoading(false)}} required/>
						  		  </div>						  		  
						  		</Form>
						  	</div>
						</div>
					</div>
					<div className="col-md-9">
						<div className="itemCardContainer">
							{loading 
							?
								<>{itemCardCategory}</> 
							:
								<div className="row" style={{"height" : "500px"}}>
									<div className="d-flex justify-content-center align-items-center">
										<Spinner animation="grow" />
									</div>
								</div>
							}
						</div>
					</div>
				</div>
				<div className="d-flex justify-content-end mb-3 mr-5">Page {pageNumber} </div>
				<div className="d-flex justify-content-end mr-5">
	            	<button className="btn btn-warning" onClick={handlePrev}>prev</button>
	            	<button className="btn btn-warning ml-2" onClick={handleNext}>next</button>       
	            </div>
			</div>
		</>
	)
}