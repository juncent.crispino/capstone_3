import {Row, Col, Jumbotron} from 'react-bootstrap'

export default function Banner() {
	return (
		<Row>
			<Col>
				<Jumbotron>
					<h1>Title</h1>
					<p>Content</p>
				</Jumbotron>
			</Col>
		</Row>
	)
}