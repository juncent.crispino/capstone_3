import { useContext, useEffect, useState } from 'react';
import { Container, Row} from 'react-bootstrap'
import UserContext from '../UserContext'
import { Link, useParams, Redirect } from 'react-router-dom'
import Reviews from '../components/Reviews'
import Swal from 'sweetalert2';
import { MdStar } from 'react-icons/md'
import Rating from 'react-rating'
import { BsHeartFill } from "react-icons/bs";


export default function SpecificCourse(){

	const [ product, setProduct ] = useState({})
	const { _id, name, brand, description, unit, unitPrice, image, availableStock, sold } = product
	const [reviews, setReviews] = useState([])
	const { user, cart, setCart } = useContext(UserContext)
	const { productId } = useParams();
	const [ itemQty, setItemQty ] = useState(1)
	const [ avgRate, setAvgRate ] = useState(0)

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProduct(data)
			setReviews(data.reviews)
		})
	}, [productId])
	
	const addToWishList = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/addToWishList/${productId}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Added to Wish List successfully',
					timer: 4000
				})
			}
		})
	}

	const subQty = () => {
		if(itemQty >= 2){
			setItemQty(itemQty - 1)
		}
	}

	const addQty = () => {
		setItemQty(itemQty + 1)
	}

	const qtyInput = (value, availableStock, unit) => {
		let inputValue = parseInt(value)
		let tempCart = [...cart]

		for(let i = 0; i < tempCart.length; i++){
			if(tempCart[i].productId === _id  && inputValue <= (availableStock - tempCart[i].quantity)){
				setItemQty(inputValue)
			}else if(tempCart[i].productId === _id && inputValue > (availableStock - tempCart[i].quantity)){
				if((availableStock - tempCart[i].quantity) === 0){
					setItemQty(1)
				}else{
					setItemQty(availableStock - tempCart[i].quantity)
				}
				Swal.fire({
					title: 'Sorry',
					icon: 'error',
					text: `Your cart already have ${tempCart[i].quantity} ${unit}/s, the available stock is only ${availableStock} ${unit}/s. You can only add ${availableStock - tempCart[i].quantity} ${unit}/s in your cart.`,
					timer: 10000
				})	
			}	
		}
	}
		
	const addToCart = (itemQty, _id, availableStock) => {
		let alreadyInCart = false
		let productIndex
		let cart = []

		if(localStorage.getItem('cart')){
			cart = JSON.parse(localStorage.getItem('cart'))
		}

		for(let i = 0; i < cart.length; i++){
			if(cart[i].productId === _id){
				alreadyInCart = true
				productIndex = i
			}
		}

		if(alreadyInCart){
			if((cart[productIndex].quantity + itemQty) <= availableStock){
				cart[productIndex].quantity += itemQty
				cart[productIndex].subtotal = cart[productIndex].unitPrice * cart[productIndex].quantity
				setItemQty(1)
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Added to Cart successfully',
					timer: 4000
				})
			}else if((cart[productIndex].quantity + itemQty) > availableStock){
				if((availableStock - cart[productIndex].quantity) === 0){
					setItemQty(1)
				}else{
					setItemQty(availableStock - cart[productIndex].quantity)
				}
				
				Swal.fire({
					title: 'Sorry',
					icon: 'error',
					text: `Your cart already have ${cart[productIndex].quantity} ${unit}/s, the available stock is only ${availableStock} ${unit}/s. You can only add ${availableStock - cart[productIndex].quantity} ${unit}/s in your cart.`,
					timer: 10000
				})
			}
			
		}else{
			if(availableStock >= 1){
				cart.push({
				'productId' : _id,
				'name': name,
				'brand': brand,
				'unit': unit,
				'unitPrice': unitPrice,
				'quantity': itemQty,
				'subtotal': unitPrice 
			})
			setItemQty(1)
			Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Added to Cart successfully',
					timer: 4000
				})
			}	
		}
		localStorage.setItem('cart', JSON.stringify(cart))
		setCart(cart)
	}

	useEffect(()=>{

		let sum = 0;
		for(let i = 0; i < reviews.length; i++){
			sum += parseFloat(reviews[i].rating)
		}

		let avg = (sum/reviews.length).toFixed(1)
		setAvgRate(avg)

	},[reviews])

	if(user.isAdmin === true){
		return <Redirect to="/products"/>
	}
	 return(
	 	<>
	 		<div className="container">
	 			<Row>
					<div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
						<Row className="mt-5 justify-content-center">
							<img  src={image} className="imgProduct" alt={name}/>
						</Row>
					</div>
					<div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12 colItems">
					<div className="text-center m-5">		
						<div  className="itemBrand">{brand}</div>
						<div className="itemName">{name}</div>
							<Rating initialRating={avgRate} readonly className="star" emptySymbol={<MdStar color={"#e4e5e9"} size={25}/>} fullSymbol={<MdStar color={"#ffc107"} size={25}/>}/>
							{reviews.length === 0 
								? <div>No reviews yet</div> 
								: <div>{avgRate}/5 Rating | {reviews.length} {reviews.length > 1 ? <>Reviews</> : <>Review</>}</div>
							}
								{sold === undefined ? <>0 </> : <>{sold}</>} {sold > 1 ? <>{unit}s</> : <>{unit}</>} Sold
						<div className="itemFont">
							<div className="mb-3"><strong>₱{unitPrice}</strong> / {unit}</div>
							<div><strong>{availableStock} {availableStock > 1 ? <>{unit}s</> : <>{unit}</>} - </strong> {availableStock > 1 ? <>Available Stocks</> : <>Available Stock</>}</div>
						</div>
						{user.id !== null 
							?
								<div className="parentbtn">
									<div className="childbtn1">
										<button  className="decbtn" onClick={(e) => subQty(e)}><strong>-</strong></button>
										<input id="quantity" type="number" min="1" value={itemQty}  className="qtyinput" onChange={(e)=> qtyInput(e.target.value, availableStock, unit)} />
										<button  className="incbtn" onClick={(e) => addQty(e)}><strong>+</strong></button>
									</div>{availableStock !== 0 
										? 	<>
											<div className="childbtn">	
												<button className="cartbtn" block onClick={(e) => addToCart(itemQty, _id, availableStock)}><strong>Add To Cart</strong></button>
											</div>
											<div className="childbtn">	
												<button className="cartbtn" block onClick={(e) => addToWishList(_id)}><strong><BsHeartFill/></strong></button>
											</div>
											</>
										: 	<div className="childbtn">	
												<button className="cartbtn" block onClick={(e) => addToWishList(_id)}><strong>Add To Wishlist</strong></button>
											</div>

									}
									
								</div>
							: <Link className="btn btn-danger btn-block" to="/login" >Log in to Buy</Link>
							}
					</div>
					</div>
				</Row>
			</div>
			<Container>
				<div className="descriptiondiv">
					<h4><strong>DESCRIPTION</strong></h4>
					{description}
				</div>
			</Container>
	 	<Reviews reviewsProp={reviews}/>	
	 	</>
	 )
}