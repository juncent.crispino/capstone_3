import { useState, useEffect, useContext} from 'react'
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import {FaShoppingCart, FaTrashAlt} from 'react-icons/fa'
import {Button, Spinner} from 'react-bootstrap'
import UserContext from '../UserContext'

export default function WishList(){
	const [wishListItems, setWishListItems] = useState([])
	const [products, setProducts] = useState([])
	const [displayItems, setDisplayItems] = useState([])
	const { user, setCart } = useContext(UserContext)
	const [loading, setLoading] = useState(false)

	const fetchProducts = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
		.then(res => res.json())
		.then(data => {
			setProducts(data)
		})
	}

	const fetchWishList = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setWishListItems(data.wishList)
			setLoading(true)		
		})
	}

	useEffect(()=> {
		fetchProducts()
		fetchWishList()
	}, [])

	useEffect(()=> {

		const addToCart = (product) => {
			let alreadyInCart = false
			let productIndex
			let cart = []

			if(localStorage.getItem('cart')){
				cart = JSON.parse(localStorage.getItem('cart'))
			}

			for(let i = 0; i < cart.length; i++){
				if(cart[i].productId === product._id){
					alreadyInCart = true
					productIndex = i
				}
			}

			if(alreadyInCart){
				if(cart[productIndex].quantity < product.availableStock){
				cart[productIndex].quantity += 1
				cart[productIndex].subtotal = cart[productIndex].unitPrice * cart[productIndex].quantity
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Added to Cart successfully',
					timer: 4000
				})
				}else if(cart[productIndex].quantity > (product.availableStock-1)){
								}
			}else{
				if(product.availableStock >= 1){
					cart.push({
						'productId' : product._id,
						'name': product.name,
						'brand': product.brand,
						'unit': product.unit,
						'unitPrice': product.unitPrice,
						'quantity': 1,
						'subtotal': product.unitPrice 
					})
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Added to Wish List successfully',
						timer: 4000
					})
				}
				
			}

			localStorage.setItem('cart', JSON.stringify(cart))
			setCart(cart)
			
		}
		const deleteWishList = (productId) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/deleteWishList/${productId}`, {
				method: "POST",
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				if(data === true){
					Swal.fire({
						title: 'Success!',
						icon: 'success',
						text: `You have successfully deleted item on wish list.`,
						timer: 5000
					})
					fetchWishList()
				}
			})
		}

		const wishListArr = products.map(item => {
			return(
				<div key={item._id}>
				{wishListItems.map(wishlist => {
						return(
							<div key={wishlist}>
								<>
									{item._id === wishlist ?	
										<>
											<div key={item._id} className="border border-dark itemCard">
												<div>
													<img className="r1" src={item.image} alt={item.name}/>
												</div>
												<div className="cardInfo">
													<div className="itemsName">
														{item.name}
													</div>
													<div className="itemsBrand">
														{item.brand}
													</div>
													<div className="soldItem">
														<strong>{item.sold} {item.unit}/s </strong>Sold
													</div>
													<div className="soldItem">
														<div>
															<strong>Php {item.unitPrice}</strong> / {item.unit}
														</div>
														<div>
															<strong>{item.availableStock} {item.unit}/s </strong>Available
														</div>
													</div>	
												</div>
												<div className="cardButton pb-3">
													<div>
														<Link size="small" className="btn btn-dark mr-2" to={`/products/${item._id}`}>Details</Link>
													</div>
													<>{user.id !== null
														? <> {item.availableStock !== 0
															? <>
																<div>
																	<Button className="btn btn-warning btn-block mx-1"  onClick={() => addToCart(item)}><FaShoppingCart className="star" color={"#343a40"} size={20}/></Button>
																</div>
																<div>
																	<Button className="btn btn-warning btn-block mx-1"  onClick={() => deleteWishList(item._id)}><FaTrashAlt className="star" color={"#343a40"} size={20}/></Button>
																</div>	
															</>
															: 
															null	
														}
															
														</>
														:<Link className="btn btn-danger btn-block" to="/login" >Log in to Buy</Link>
														}		
													</>
												</div>
											</div>		   	
										</> : null
									}		
								</>
								
							</div>)
					})
				}	
				</div>
				
			)
		})

		setDisplayItems(wishListArr)

	},[wishListItems, products, user, setCart])

	return (
		<div className="itemCardContainer">
			{loading ? 
				<>{displayItems}</>
			: 
			<>
				<div className="row" style={{"height" : "500px"}}>
					<div className="d-flex justify-content-center align-items-center">
						<Spinner animation="grow" />
					</div>
				</div>
			</>
			}
		</div>
	)
}