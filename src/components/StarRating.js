import React, {useState} from 'react'
import { MdStar } from 'react-icons/md'

export default function StarRating() {

	const [rating, setRating] = useState(null)
	const [hover, setHover] = useState(null)
	return (
		<div>
			{[...Array(5)].map((star, i) => {
				const ratingValue = i + 1;

				return (
					<label key={ratingValue}>
						<input type="radio" name="rating" value={ratingValue} />
						<MdStar className="star" color={ratingValue <= (hover ||rating) ? "#ffc107" : "#e4e5e9"} size={75} onClick={() => setRating(ratingValue)} onMouseEnter={()=>setHover(ratingValue)} onMouseLeave={()=> setHover(null)}/>
					</label>
				)
			})}	

		</div>
	)
}