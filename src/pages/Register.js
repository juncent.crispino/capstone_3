import React, { useState, useEffect } from 'react';
import { Form, Button, Container } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Register() {


	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [verifyPassword, setVerifyPassword] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [address, setAddress] = useState('')
	const history = useHistory()


	const[registerButton, setRegisterButton] = useState(false)

	function registerUser(e){
		e.preventDefault()

		fetch(`${ process.env.REACT_APP_API_URL }/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === false){
				inputUserDetails()
				history.push('/login')

			}else{
				return Swal.fire({
						title: 'Email already exist',
						icon: 'error',
						text: 'Please use other email'
					})
			}
		})
	}

	const inputUserDetails = () => {
		fetch(` ${ process.env.REACT_APP_API_URL }/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
				address: address
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
			 return	Swal.fire({
						title: 'Yaaaaaaay!!!',
						icon: 'success',
						text: 'You have successfully register'
					})
			}else{
				return Swal.fire({
						title: 'Sorry',
						icon: 'error',
						text: 'Check your register details and try again'
					})
			}
		})
	}
	useEffect(() => {

		if((email !== '' && password !=='' && verifyPassword !=='') && (password === verifyPassword)){
			setRegisterButton(true)
		}else{
			setRegisterButton(false)
		}

	}, [email, password, verifyPassword])

	return(
		<Container>
			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>

			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" value={firstName}  onChange={e => setFirstName(e.target.value)} required/>
			</Form.Group>				
			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" value={lastName}  onChange={e => setLastName(e.target.value)} required/>
			</Form.Group>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Verify Password:</Form.Label>
					<Form.Control type="password" placeholder="Verify Password" value={verifyPassword}  onChange={e => setVerifyPassword(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="number" placeholder="Enter Last Name" value={mobileNo}  onChange={e => setMobileNo(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Address</Form.Label>
					<Form.Control type="text" placeholder="Enter Address" value={address}  onChange={e => setAddress(e.target.value)} required/>
				</Form.Group>

				{registerButton ?
  
					<Button variant="warning" type="submit" >Submit</Button>
				:

					<Button variant="warning" type="submit" disabled>Submit</Button>
				}	
			</Form>
		</Container>
	)
}